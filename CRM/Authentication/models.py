from django.db import models
from django.urls import reverse
# Create your models here.



class Education(models.Model):
    name = models.CharField(max_length = 255)

    def __str__(self):
        return self.name
    


class Role(models.Model):
    name = models.CharField(max_length = 255)
    discount = models.IntegerField


class Permission(models.Model):
    name = models.CharField(max_length = 255)


class Validation(models.Model):
    username = models.CharField(max_length = 255)
    password = models.CharField(max_length = 255)
    first_name = models.CharField(max_length = 255 , null = True)
    last_name = models.CharField(max_length = 255, null = True)
    birthday = models.DateField(null = True)
    address = models.CharField(max_length = 255, null = True)
    education = models.ForeignKey(Education , on_delete = models.CASCADE, null = True)
    picture_url = models.CharField(max_length = 255, null = True)
    email = models.CharField(max_length = 255, null = True)
    mobile = models.CharField(max_length = 255, null = True)
    phone = models.CharField(max_length = 255, null = True)
    code = models.CharField(max_length = 255)


class RolePermission(models.Model):
    role = models.ForeignKey(Role , on_delete = models.CASCADE)
    permission = models.ForeignKey(Permission, on_delete=models.CASCADE)



class User(models.Model):
    username = models.CharField(max_length = 255)
    password = models.CharField(max_length = 255)
    first_name = models.CharField(max_length = 255 , null = True)
    last_name = models.CharField(max_length = 255, null = True)
    birthday = models.DateField(null = True)
    address = models.CharField(max_length = 255, null = True)
    education = models.ForeignKey(Education , on_delete = models.CASCADE, null = True)
    picture_url = models.CharField(max_length = 255, null = True)
    email = models.CharField(max_length = 255, null = True)
    mobile = models.CharField(max_length = 255, null = True)
    phone = models.CharField(max_length = 255, null = True)
    role = models.ForeignKey(Role , on_delete = models.CASCADE)
