from django.shortcuts import render,HttpResponse
from Authentication.models import User,Education,Validation
from random import randint
# Create your views here.

def login(request):
    if request.method == "POST":
        try:
            user = User.objects.get(username=request.POST['username'])
            if user.password == request.POST['password']:
                request.session['user_id'] = user.id
                return HttpResponse("Dashboard")
            else:
                content = {
                    'is_exists' : False
                }
                return render(request,'Autentication/login.html',content)
        except User.DoesNotExist:
            content = {
                'is_exists' : False
            }
            return render(request,'Autentication/login.html',content)
    else:
        content = {
                'is_exists' : True
            }
        return render(request,'Autentication/login.html',content)


def logout(request):
    try:
        del request.session['user_id']
    except KeyError:
        pass
    content = {
                'is_exists' : True
            }
    return render(request,'Autentication/login.html',content)


def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        phone = request.POST['phone']
        if check_send_params_not_empty(username,password,email,phone) == False:
            content = {
                'has_err':True,
                "err_key":1 #Empty Fields
            }
            return render(request,'Autentication/register.html',content)
        

        if check_user_not_exists(username):
            if phone != "":
                active_code = randint(100, 999)
                validation = Validation.objects.create(username=username,
                password = password,
                email = email,
                phone = phone,
                code = active_code)
                return HttpResponse("Dashboard")

                #TODO : SEND SMS
            elif email != "":
                active_code = randint(10000, 99999)
                validation = Validation.objects.create(username=username,
                password = password,
                email = email,
                phone = phone,
                code = active_code)
                #TODO : SEND EMAIL
                return HttpResponse("Dashboard")

            
        else:
            content = {
                'has_err':True,
                "err_key":2 #USER EXISTS
            }
            return render(request,'Autentication/register.html',content)


        

    else:
        return render(request,'Autentication/register.html')

def check_send_params_not_empty(username,password,email,phone):
    if username == "" or password == "" or (email == "" and phone == ""):
        return False
    else:
        return True

def check_user_not_exists(username):        
    try:
        user = User.objects.get(username = username)
        return False
    except User.DoesNotExist:
        try:
            valid = Validation.objects.get(username = username)
        except Validation.DoesNotExist:
            return True

