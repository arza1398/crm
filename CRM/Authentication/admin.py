from django.contrib import admin
from .models import Education,Permission,Role,RolePermission

# Register your models here.

class EducationAdmin(admin.ModelAdmin):
    list_display = ('name')


class PermissionAdmin(admin.ModelAdmin):
    list_display = ('name')


class RoleAdmin(admin.ModelAdmin):
    list_display = ('name')
    
class RolePermissionAdmin(admin.ModelAdmin):
    list_display = ('name')

admin.site.register(Education)

admin.site.register(Permission)

admin.site.register(Role)

admin.site.register(RolePermission)
