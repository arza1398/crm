from django import forms

from .models import Document


class Document(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('title','document')
