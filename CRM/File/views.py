from django.shortcuts import render,HttpResponse,redirect
from .forms import Document
from Product.models import Product

# Create your views here.


def upload_document(request):
    if request.method == 'POST':
        form = Document(request.POST, request.FILES)
        if form.is_valid():
            document = form.save(commit=False)
            product = Product.objects.get(id = 2)
            document.product_id = product.id
            document.save()
            return render(request, 'upload_document.html')
    else:
        form = Document()
    return render(request, 'upload_document.html', {
        'form': form
    })

def get_image(request):
    return HttpResponse("image")

def delete_image(request):
    return HttpResponse("image")