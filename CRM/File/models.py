from django.db import models
from Product import models as prod
from django.urls import reverse
# Create your models here.

class Image(models.Model):
    product = models.ForeignKey(prod.Product , on_delete = models.CASCADE)
    url = models.CharField(max_length = 255)
    

    def __str__(self):
        return self.url

    def get_absolute_url(self):
        return reverse("Image_detail", kwargs={"pk": self.pk})


class Video(models.Model):
    product = models.ForeignKey(prod.Product , on_delete = models.CASCADE)
    url = models.CharField(max_length = 255)
    

    def __str__(self):
        return self.url

    def get_absolute_url(self):
        return reverse("Video_detail", kwargs={"pk": self.pk})


class Document(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    document = models.FileField(upload_to='files/')
    product = models.ForeignKey(prod.Product , on_delete = models.CASCADE)
    # cover = models.ImageField(upload_to='books/covers/', null=True, blank=True)

    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        self.pdf.delete()
        self.cover.delete()
        super().delete(*args, **kwargs)
