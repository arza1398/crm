from django.contrib import admin
from django.urls import path,include
from Product import views as prod

app_name = 'main'
urlpatterns = [
    # Category
    path('category/all', prod.get_all_categorys),
    path('category/get', prod.get_category),
    path('category/add', prod.add_category),
    path('category/update', prod.update_category),
    path('category/delete', prod.delete_category),
    # Group
    path('group/all', prod.get_all_groups),
    path('group/get', prod.get_group),
    path('group/add', prod.add_group),
    path('group/update', prod.update_group),
    path('group/delete', prod.delete_group),
    # Row
    path('row/all', prod.get_all_rows),
    path('row/get', prod.get_row),
    path('row/add', prod.add_row),
    path('row/update', prod.update_row),
    path('row/delete', prod.delete_row),
 # Product
    path('all', prod.get_all_products),
    path('get', prod.get_product),
    path('add', prod.add_product),
    path('update', prod.update_product),
    path('delete', prod.delete_product)
]
