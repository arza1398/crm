from django.db import models
from Authentication import models as auth
from django.urls import reverse

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length = 255)
    description = models.CharField(max_length = 255,null = True)
    
    
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Category_detail", kwargs={"pk": self.pk})


class Group(models.Model):
    name = models.CharField(max_length = 255)
    description = models.CharField(max_length = 255,null = True)
    category = models.ForeignKey(Category , on_delete = models.CASCADE)
    
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Group_detail", kwargs={"pk": self.pk})


class Row(models.Model):
    name = models.CharField(max_length = 255)
    description = models.CharField(max_length = 255,null = True)
    group = models.ForeignKey(Group , on_delete = models.CASCADE)
        
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Row_detail", kwargs={"pk": self.pk})
    

class Product(models.Model):
    name = models.CharField(max_length =255)
    description = models.CharField(max_length = 255,null = True)
    price = models.IntegerField()
    row = models.ForeignKey(Row , on_delete = models.CASCADE)
    seller = models.ForeignKey(auth.User , on_delete = models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Product_detail", kwargs={"pk": self.pk})

