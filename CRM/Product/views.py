from django.shortcuts import render,HttpResponse
from .models import Category,Group,Row,Product
from django.http import JsonResponse
import json
from Authentication import views as auth
from Authentication.models import User

# Create your views here.

#Category
def get_all_categorys(request):
        if request.method == 'GET':       
                try:
                        categorys = Category.objects.get().all()
                except Category.DoesNotExist:
                        return HttpResponse("Category Does Not Exist")
                content = []
                for category in categorys:
                        temp = {
                                'id' : category.id,
                                'name' : category.name,
                                'description' : category.description
                        }
                        content.append(temp)

                return JsonResponse(content,safe=False)

def get_category(request):
    if request.method == 'GET':
        id = request.GET['id']
        
        try:
                category = Category.objects.get(id=id)
        except Category.DoesNotExist:
                return HttpResponse("Category Does Not Exist")

        content = {
                'id' : category.id,
                'name' : category.name,
                'description' : category.description
        }
        return JsonResponse(content,safe=False)

def add_category(request):
    if request.method == 'POST':
        name = request.POST['name']
        description = request.POST['description']
        try:
                category = Category.objects.create(name=name,description=description)
                return HttpResponse("Category Added")
        except Exception:
                return  HttpResponse("Error, Category Does Not Creat")

def delete_category(request):
    if request.method == 'POST':
        id = request.POST['id']
        try:
                Category.objects.get(id= id).delete()
                return HttpResponse("Category Deleted")
        except Exception:
                return HttpResponse("Category Not Deleted")

def update_category(request):
    if request.method == 'POST':
        id = request.POST['id']
        name = request.POST['name']
        description = request.POST['description']

        try:
                category = Category.objects.get(id= id)
                category.name = name
                category.description = description
                category.save()
                return HttpResponse("Category Updated")
        except Exception:
                return HttpResponse("Category Not Updated")



#Group
def get_all_groups(request):
        if request.method == 'GET':       
                try:
                        groups = Group.objects.all()
                except Group.DoesNotExist:
                        return HttpResponse("Group Does Not Exist")
                content = []
                for group in groups:
                        temp = {
                                'id' : group.id,
                                'name' : group.name,
                                'description' : group.description,
                                'category' : {
                                        'id':group.category.id,
                                        'name' :group.category.name
                                        }
                                }
                        content.append(temp)

                return JsonResponse(content,safe=False)

def get_group(request):
    if request.method == 'GET':
        id = request.GET['id']
        
        try:
                group = Group.objects.get(id=id)
        except Group.DoesNotExist:
                return HttpResponse("Group Does Not Exist")

        content = {
                'id' : group.id,
                'name' : group.name,
                'description' : group.description,
                'category' : {
                        'id':group.category.id,
                        'name' :group.category.name
                        }
                }
        return JsonResponse(content,safe=False)

def add_group(request):
    if request.method == 'POST':
        name = request.POST['name']
        description = request.POST['description']
        category_id = request.POST['category_id']
        try:
                category = Category.objects.get(id = category_id)
                group = Group.objects.create(name=name,description=description,category=category)
                return HttpResponse("Group Added")
        except Exception as e:
                return  HttpResponse("Error, Group Does Not Creat " + str(e))

def delete_group(request):
    if request.method == 'POST':
        id = request.POST['id']
        try:
                Group.objects.get(id = id).delete()
                return HttpResponse("Group Deleted")
        except Exception:
                return HttpResponse("Group Not Deleted")

def update_group(request):
    if request.method == 'POST':
        id = request.POST['id']
        name = request.POST['name']
        description = request.POST['description']
        category_id = request.POST['category_id']
        try:
                category = Category.objects.get(id = category_id)
                group = Group.objects.get(id= id)
                group.name = name
                group.description = description
                group.category = category
                group.save()
                return HttpResponse("Group Updated")
        except Exception:
                return HttpResponse("Group Not Updated")



#Row
def get_all_rows(request):
        if request.method == 'GET':       
                try:
                        rows = Row.objects.all()
                except Row.DoesNotExist:
                        return HttpResponse("Row Does Not Exist")
                content = []
                for row in rows:
                        temp = {
                                'id' : row.id,
                                'name' : row.name,
                                'description' : row.description,
                                'category' : {
                                        'id':row.group.id,
                                        'name' :row.group.name
                                        }
                                }
                        content.append(temp)

                return JsonResponse(content,safe=False)

def get_row(request):
    if request.method == 'GET':
        id = request.GET['id']
        
        try:
                row = Row.objects.get(id=id)
        except Row.DoesNotExist:
                return HttpResponse("Row Does Not Exist")

        content = {
                'id' : row.id,
                'name' : row.name,
                'description' : row.description,
                'category' : {
                        'id':row.group.id,
                        'name' :row.group.name
                        }
                }
        return JsonResponse(content,safe=False)

def add_row(request):
    if request.method == 'POST':
        name = request.POST['name']
        description = request.POST['description']
        group_id = request.POST['group_id']
        try:
                group = Group.objects.get(id = group_id)
                row = Row.objects.create(name=name,description=description,group=group)
                return HttpResponse("Row Added")
        except Exception as e:
                return  HttpResponse("Error, Row Does Not Creat " + str(e))

def delete_row(request):
    if request.method == 'POST':
        id = request.POST['id']
        try:
                Row.objects.get(id = id).delete()
                return HttpResponse("Row Deleted")
        except Exception:
                return HttpResponse("Row Not Deleted")

def update_row(request):
    if request.method == 'POST':
        id = request.POST['id']
        name = request.POST['name']
        description = request.POST['description']
        group_id = request.POST['group_id']
        try:
                group = Group.objects.get(id = group_id)
                row = Row.objects.get(id= id)
                row.name = name
                row.description = description
                row.group = group
                row.save()
                return HttpResponse("Row Updated")
        except Exception as e:
                return HttpResponse("Row Not Updated" + str(e))

#Product
def get_all_products(request):
        if request.method == 'GET':       
                try:
                        products = Product.objects.all()
                except Row.DoesNotExist:
                        return HttpResponse("product Does Not Exist")
                content = []
                for product in products:
                        temp = {
                                'id' : product.id,
                                'name' : product.name,
                                'description' : product.description,
                                'price' : product.price,
                                'quantity' : product.quantity,

                                'row' : {
                                        'id': product.row.id,
                                        'name' : product.row.name
                                        },
                                'seller' : {
                                        'id': product.seller.id,
                                        'name' : product.seller.first_name + ' ' + product.seller.last_name
                                        }        
                                }
                        content.append(temp)

                return JsonResponse(content,safe=False)

def get_product(request):
    if request.method == 'GET':
        id = request.GET['id']
        
        try:
                product = Product.objects.get(id=id)
        except product.DoesNotExist:
                return HttpResponse("product Does Not Exist")

        content = {
               'id' : product.id,
                'name' : product.name,
                'description' : product.description,
                'price' : product.price,
                'quantity' : product.quantity,
                'row' : {
                        'id': product.row.id,
                        'name' : product.row.name
                        },
                'seller' : {
                        'id': product.seller.id,
                        'name' : product.seller.first_name +' '+ product.seller.last_name
                        }        
                }
        return JsonResponse(content,safe=False)

def add_product(request):
    if request.method == 'POST':
        name = request.POST['name']
        description = request.POST['description']
        price = request.POST['price']
        quantity = request.POST['quantity']
        row_id = request.POST['row_id']
        seller_id = request.POST['seller_id']

        try:
                row = Row.objects.get(id = row_id)
                seller = User.objects.get(id = seller_id)
                product = Product.objects.create(name = name, description = description, price = price,
                 quantity = quantity, row_id = row_id, seller_id = seller_id)
                return HttpResponse("Product Added")
        except Exception as e:
                return  HttpResponse("Error, Product Does Not Creat " + str(e))

def delete_product(request):
    if request.method == 'GET':
        id = request.POST['id']
        try:
                Product.objects.get(id = id).delete()
                return HttpResponse("Product Deleted")
        except Exception:
                return HttpResponse("Product Not Deleted")

def update_product(request):
    if request.method == 'POST':
        id = request.POST['id']
        name = request.POST['name']
        description = request.POST['description']
        price = request.POST['price']
        quantity = request.POST['quantity']
        row_id = request.POST['row_id']
        seller_id = request.POST['seller_id']
        try:
                row = Row.objects.get(id = row_id)
                seller = User.objects.get(id = seller_id)
                product = Product.objects.get(id= id)
                product.name = name
                product.description = description
                product.price = price
                product.quantity = quantity
                product.row_id = row_id
                product.seller_id = seller_id
                product.save()
                return HttpResponse("Product Updated")
        except Exception as e:
                return HttpResponse("Product Not Updated" +' '+ str(e))

